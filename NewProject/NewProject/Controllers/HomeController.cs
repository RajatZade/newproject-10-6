﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewProject.Controllers
{
    public class HomeController : Controller
    {
        static int MAX = 1005;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult Squarefree(int n)
        {
                List<int> primes = new List<int>();
                SieveOfEratosthenes(primes);
                int max_count = 0;
                for (int i = 0; i < primes.Count && primes[i] * primes[i] <= n; i++)
                {
                    if (n % primes[i] == 0)
                    {
                        int tmp = 0;
                        while (n % primes[i] == 0)
                        {
                            tmp++;
                            n /= primes[i];
                        }
                        max_count = Math.Max(max_count, tmp);
                    }
                }

                if (max_count == 0)
                {
                    max_count = 1;
                }
            return Json(max_count);
        }

        static void SieveOfEratosthenes(List<int> primes)
        {
  
            bool[] prime = new bool[MAX];
            for (int i = 0; i < prime.Length; i++)
            {
                prime[i] = true;
            }
            for (int p = 2; p * p < MAX; p++)
            {

                if (prime[p] == true)
                {
                    for (int i = p * 2; i < MAX; i += p)
                    {
                        prime[i] = false;
                    }
                }
            }

            for (int p = 2; p < MAX; p++)
            {
                if (prime[p])
                {
                    primes.Add(p);
                }
            }
        }
    }
}